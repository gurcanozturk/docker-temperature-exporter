# Dockerized temperature exporter for prometheus. 

Find your USB thermometer device address.

```
root@pve:~/docker-temp-exporter# lsusb | grep -i temperature
Bus 001 Device 002: ID 0c45:7401 Microdia TEMPer Temperature Sensor
```

# Build it
docker build -t temperature-exporter .

# Run it
docker run -d --name temp-exporter -p 5000:5000 -e APP_PORT=5000 --device /dev/bus/usb/001/002:/dev/temper --privileged temperature-exporter

# Check it.
```
root@pve:~/docker-temp-exporter# curl http://192.168.1.3:5000/metrics

# HELP temperature Temperature value read via USB thermometer
# TYPE temperature gauge
temperature{id="temper"} 27.10
```

# Graph it
Edit new panel in your Grafana and graph it.