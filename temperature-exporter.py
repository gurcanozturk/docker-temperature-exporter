#!/usr/bin/python3

import subprocess
from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/metrics')
def index():
  commandos = subprocess.run(["temper-poll", "-c", "-s0"], capture_output=True)
  if commandos.returncode == 0:
     temp = float(commandos.stdout.decode().rstrip())

     line = """# HELP temperature Temperature value read via USB thermometer
# TYPE temperature gauge
temperature{id=\"temper\"}
"""
     line = line.rstrip() + (" %.2f" % (temp))
     return line

  else:
    print("Houston! We have a problem!")

if __name__ == '__main__':
   app.run(host='0.0.0.0')