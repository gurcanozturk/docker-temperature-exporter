FROM alpine:latest

COPY ./run.sh /
COPY ./temperature-exporter.py /

RUN chmod +x /temperature-exporter.py && \
    chmod +x /run.sh && \
    apk update && \
    apk --no-cache add \
        libusb \
        python3 \
        py3-pip \
        py3-msgpack && \
        pip install temperusb && \
        pip install flask && \
        rm -rf /var/cache/apk/*

ENTRYPOINT ["/run.sh"]